using UnrealBuildTool;
using System.Collections.Generic;

public class BasicCxxEditorTarget : TargetRules
{
	public BasicCxxEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		
		ExtraModuleNames.AddRange( new string[] { "BasicCxx" } );
	}
}
